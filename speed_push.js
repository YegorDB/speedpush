const squares = [
    [1, 3], [2, 3], [3, 3],
    [1, 2], [2, 2], [3, 2],
    [1, 1], [2, 1], [3, 1],
];
const allAdditionalClasses = [
    "target",
    "trap",
    "marked",
].join(" ");
var points = 0;
var reward = 1;


function getSquarePlace(allSquares) {
    let index = Math.floor(Math.random() * allSquares.length);
    return allSquares.splice(index, 1)[0];
}


function getSquareObject(x, y) {
    return $(`.square[x=${x}][y=${y}]`);
}


function clearAdditionalClasses(squareObject) {
    squareObject.removeClass(allAdditionalClasses);
}


function highlightSquare(squarePlace, additionalClasses) {
    let squareObject = getSquareObject(...squarePlace);
    squareObject.addClass(additionalClasses);
    setTimeout(() => {clearAdditionalClasses(squareObject)}, 800);
}


function highlightSquares() {
    let squaresCopy = squares.slice();
    if (Math.random() < 0,5) { // with trap
        let [first, second] = Math.random() < 0.5 ? ["target", "trap"] : ["trap", "target"];
        highlightSquare(getSquarePlace(squaresCopy), `${first} marked`);
        setTimeout(() => {highlightSquare(getSquarePlace(squaresCopy), `${second} marked`);}, 200);
    }
    else {
        highlightSquare(getSquarePlace(squaresCopy), "target marked");
    }
}


var intervalId = setInterval(highlightSquares, 1000);
setTimeout(() => {clearInterval(intervalId);}, 10000);


$('#board')
.delegate('.square.target', 'click', function() {
    points += reward;
    reward++;
    $("#points").text(points);
    $("#reward").text(reward);
})
.delegate('.square.trap', 'click', function() {
    reward = 1;
    $("#reward").text("1");
})
